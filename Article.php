<?php

class Article extends Publication
{
    public $author;

    public function __construct($id, $type, $shortText, $fullText, $author)
    {
        parent::__construct($id, $type, $shortText, $fullText);
        $this->author = $author;
    }

    public function getShortPreview()
    {
        return '<h3>' . $this->shortText . '</h3>' .
            '<a href="Preview.php?id=' . $this->getID() . '" class="btn btn-primary" role="button">Читать полностью</a>';
    }
}