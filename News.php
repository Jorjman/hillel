<?php

class News extends Publication
{
    public $source;

    public function __construct($id, $type, $shortText, $fullText, $source)
    {
        parent::__construct($id, $type, $shortText, $fullText);
        $this->source = $source;
    }

    public function getShortPreview()
    {
        return '<h3>' . $this->shortText . '</h3>' .
            '<a href="Preview.php?id=' . $this->getID() . '" class="btn btn-primary" role="button">Читать полностью</a>';
    }


}