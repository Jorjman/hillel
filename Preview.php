<?php
require_once('connect_db.php');
require_once('Publication.php');
require_once('PublicationsWriter.php');

try {
    if (isset($_GET['id'])) {
        $id = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');
    }
    if ($id <= 0) {
        throw new Exception('Неверный ID');
    }
    $publication = Publication::create($id, $pdo);
    $publication->getPreview();

} catch (Exception $exception) {
    echo $exception->getMessage();
}