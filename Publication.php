<?php

abstract class Publication
{
    public $id;
    public $type;
    public $shortText;
    public $fullText;

    public function __construct($id, $type, $shortText, $fullText)
    {
        $this->id = $id;
        $this->type = $type;
        $this->shortText = $shortText;
        $this->fullText = $fullText;
    }

    public static function create($id, PDO $pdo)
    {
        if ($id <= 0) {
            throw new Exception('Неверный ID');
        }
        if (!is_a($pdo, 'PDO')) {
            throw new Exception('Не указан PDO');
        }
        $query = "SELECT * FROM Publication WHERE id=:id";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $row = $stmt->fetchObject();
        if (empty($row)) {
            return null;
        }
        if ($row->type == 'news') {
            $publication = new News(
                $row->id,
                $row->type,
                $row->shortText,
                $row->fullText,
                $row->source
            );
        } else {
            $publication = new Article(
                $row->id,
                $row->type,
                $row->shortText,
                $row->fullText,
                $row->author
            );
        }
        $publication->setID($row->id);
        return $publication;
    }

    public function getPreview()
    {
        echo $this->fullText;
        if ($this->type == 'news') {
            echo '<br>' . 'Источник: ' . $this->source;
        } else {
            echo '<br>' . 'Автор: ' . $this->author;
        }
        echo '<hr>';
        echo '<a href="index.php">На главную</a>';
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getID()
    {
        return $this->id;
    }

}