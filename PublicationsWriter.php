<?php

require_once 'Article.php';
require_once 'News.php';

class PublicationsWriter
{

    public $publications = [];

    public function __construct($type, $pdo)
    {

        $sql = "SELECT * FROM Publication WHERE type = :type";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':type', $type);
        $stmt->execute();
        $rows = $stmt->fetchAll();

        foreach ($rows as $row) {
            if ($row['type'] == 'news') {
                $this->publications[] = new News(
                    $row['id'],
                    $row['fullText'],
                    $row['shortText'],
                    $row['type'],
                    $row['source']
                );
            } elseif ($row['type'] == 'article') {
                $this->publications[] = new Article(
                    $row['id'],
                    $row['fullText'],
                    $row['shortText'],
                    $row['type'],
                    $row['author']
                );
            } else {
                throw new Exception('Неверный тип записи');
            }
        }
        return null;
    }

}



