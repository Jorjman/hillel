<?php
require_once('connect_db.php');
require_once('Publication.php');
require_once('News.php');
require_once('Article.php');
require_once('PublicationsWriter.php');
try {
    $article = new PublicationsWriter('article', $pdo);
    $news = new PublicationsWriter('news', $pdo);
} catch (Exception $e) {
    echo $e->getMessage();
}

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>HW17</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>

<style>
    h2 {
        margin: 8px;
    }

    #left {
        margin: 8px;
    }

    #right {
        margin: 8px;
    }
</style>

<body>
<h2>Home_Work <span class="label label-default">#17</span></h2>
<hr>

<div id="left">
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <?php foreach ($news->publications as $newss): ?>
                    <div class="caption">
                        <?= $newss->getShortPreview() ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<div id="right">
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <?php foreach ($article->publications as $articles): ?>
                    <div class="caption">
                        <?= $articles->getShortPreview() ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
</script>
</body>
</html>

